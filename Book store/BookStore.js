//Created an array for authors
let listOfAllKnownAuthors = []

//Creating a bookstore
class BookStore
{
    //taking name , adress and owner of the bookstore
    constructor(name, address, owner)
    {
        //Making private
        this._name = name;
        this._address = address;
        this._owner = owner;
        this._booksAvailable = [];
        this._totalCopiesOfAllBooks = 0
    }

    //Finding the booki in the bookstore using author name
    authorKnown(authorName)
    {
        //Intially declaring false
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                //If author is found is found , making it true
                foundThem = true
            }
        }
        return foundThem
    }
    addBook(bookInstance, copies)
        if (positionOfBook != null)
        {
             let foundBook = this._booksAvailable[positionOfBook];
             //Adding the new copies with old copies
             foundBook.copies += copies;
             console.log("Added " + copies + " copies of " + foundBook.book);
             listOfAllKnownAuthors.push(foundBook.book.author);
        }
        else
    //Adding the book
    addBook(bookInstance, copies)
    {
        let positionOfBook = this.checkForBoo
        {
             let bookCopies = {
                 book: bookInstance,
                 copies: copies
             };
             this._booksAvailable.push(bookCopies);
             console.log("Added " + copies + " copies of a new book: " + bookInstance);
        }

        this._totalCopiesOfAllBooks += copies;
        }
        console.log("copies can't be in negative number")
    }

    //If book was sold
    sellBook(bookInstance, numberSold)
    {
        let positionOfBook = this.checkForBook(bookInstance);
        if (positionOfBook != null)
        {
            let foundBook = this._booksAvailable[positionOfBook];
            //Making sure their are certain number of copies
            if (numberSold > this._booksAvailable[positionOfBook].copies)
            {
                console.log("Not enough copies of " + foundBook.book + " to sell");
            }
            else
            {
                //Subtracting from the toatal copies of the specific book
                foundBook.copies -= numberSold;
                if (foundBook.copies === 0)
                {
                    this._booksAvailable.pop(PositionOfBook);
                    this._NumTitles -= 1;
                    let foundAuth = this.authorKnown(foundBook.book.author);
                    listOfAllKnownAuthors.pop(foundAuth);
                }
                //Subtracting the copies of all books in the stores
                this._totalCopiesOfAllBooks -= numberSold;
                console.log("Sold " + numberSold + " copies of " + foundBook.book);
            }
        }
        else
        {
            console.log(bookInstance + " not found");
        }
    }

    //Checking for the book
    checkForBook(bookInstance)
    {
        let currBookNum = 0;
        while (currBookNum < this._booksAvailable.length)
        {
            if (this._booksAvailable[currBookNum].book.isTheSame(bookInstance))
            {
                return currBookNum;
            }
            else
            {
                
            }
            currBookNum += 1;
        }
        return null;
    }

    //Returning name
    get name()
    {
        return this._name;
    }

    //Setting a new name
    set name(newName)
    {
        this._name = newName;
    }

    //Getting the adress
    get address()
    {
        return this._address;
    }

    //Setting the adress
    set address(newAddress)
    {
        this._address = newAddress;
    }

    //Getting the owner
    get owner()
    {
        return this._owner;
    }

    //Setting adress of new owner
    set owner(newOwner)
    {
        this._owner = newOwner;
    }
}

//Created a new class for book
class Book
{
    //Book details
    constructor(title, author, publicationYear, price)
    {
        this._title = title;
        this._author = author;
        this._publicationYear = publicationYear;
        this._price = price;
        //pushing the author name if not in in the array of authors
        if (this.authorKnown(this._author) === false)
        {
            listOfAllKnownAuthors.push(this._author)
        }
    }

    //Setting the same price for the same book
    isTheSame(otherBook)
    {
        return otherBook.price === this.price;
    }

    //chekcing with the author
    authorKnown(authorName)
    {
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true;
            }
        }
        return foundThem;
    }

    //returning the title
    get title()
    {
        return this._title;
    }

    //Returning the author
    get author()
    {
        return this._author;
    }

    //Returning the Year of the publication of the book
    get publicationYear()
    {
        return this._publicationYear;
    }

    //Returnong the price
    get price()
    {
        return this._price;
    }

    //Setting the stirng of details
    toString()
    {
        return this.title + ", " + this.author + ". " + this.publicationYear + " ($" + this.price + ")";
    }
}

// Book details courtesy of Harry Potter series by J.K. Rowling
let cheapSpellBook = new Book("The idiot's guide to spells","Morlan",2005,40);
let flourishAndBlotts = new BookStore("Flourish & Blotts", "North side, Diagon Alley, London, England", "unknown");
let monsterBook = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let monsterBookToSell = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let spellBook = new Book("The Standard Book of Spells, Grade 4", "Miranda Goshawk", 1921, 80);
flourishAndBlotts.addBook(cheapSpellBook,1000);
flourishAndBlotts.addBook(monsterBook, 500);
flourishAndBlotts.sellBook(monsterBookToSell, 200);
flourishAndBlotts.addBook(spellBook, 40);
flourishAndBlotts.addBook(spellBook, 20);
flourishAndBlotts.sellBook(spellBook, 15);
flourishAndBlotts.addBook(monsterBookToSell, -30);
flourishAndBlotts.sellBook(monsterBookToSell, 750);

console.log("Authors known: " + listOfAllKnownAuthors);
