"use strict";

var formIDs = ["address", "roomNumber", "seatsUsed", "seatsTotal"];
var formValues = ['Building address...', 'Room number', 'Number of seats in use', 'Number of available seats'];
var formInput = [false, true, true, true];
function resetForm() {
    document.location.reload(true);
}
function AutomaticAddress() {
    console.log(document.getElementById("useAddress").value)
    var checkbox = document.getElementById("useAddress");
    if (checkbox.checked == true) {
        ///////////////
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = pos.coords;

            console.log('Your current position is:');
            console.log(`Latitude : ${crd.latitude}`);
            console.log(`Longitude: ${crd.longitude}`);
            console.log(`More or less ${crd.accuracy} meters.`);

            //////////////
            var latitude = crd.latitude;
            var longitude = crd.longitude;
            var api_url = 'https://api.opencagedata.com/geocode/v1/json'

            var request_url = api_url
                + '?'
                + 'key=' + apikey
                + '&q=' + encodeURIComponent(latitude + ',' + longitude)
                + '&pretty=1'
                + '&no_annotations=1';

            // see full list of required and optional parameters:
            // https://opencagedata.com/api#forward

            var request = new XMLHttpRequest();
            request.open('GET', request_url, true);

            request.onload = function () {
                // see full list of possible response codes:
                // https://opencagedata.com/api#codes

                if (request.status == 200) {
                    // Success!
                    var data = JSON.parse(request.responseText);
                    alert(data.results[0].formatted);
                    document.getElementById("address").value = data.results[0].formatted

                } else if (request.status <= 500) {
                    // We reached our target server, but it returned an error

                    console.log("unable to geocode! Response code: " + request.status);
                    var data = JSON.parse(request.responseText);
                    console.log(data.status.message);
                } else {
                    console.log("server error");
                }
            };

            request.onerror = function () {
                // There was a connection error of some sort
                console.log("unable to connect to server");
            };

            request.send();

        }

        function error(err) {
            console.warn(`ERROR(${err.code}): ${err.message}`);
        }
        ///////////////////////////////////


        navigator.geolocation.getCurrentPosition(success, error, options);
        /////////////////
        console.log("NewObservation")


    }

}
function saveForm() {
    var check = 0;
    for (var i = 0; i < formIDs.length; i++) {
        if (document.getElementById(formIDs[i]).value == formValues[i]) {
            alert(formIDs[i] + "not entered")
        }
        else if (isNaN(document.getElementById(formIDs[i]).value) == formInput[i]) {
            alert(formValues[i] + "is not a valid input")

        } else if (document.getElementById("roomNumber").value < 0 || document.getElementById("seatsUsed").value < 0 || document.getElementById("seatsTotal").value < 0) {
            alert("Input can't be negative")
        } else {
            check++
        }
        if (check == formIDs.length) {

            console.log(document.getElementById("roomNumber").value)

            var datetime = currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds()+ " @ "+ currentdate.getDate() + "/"
                + (currentdate.getMonth() + 1) + "/"
                + currentdate.getFullYear() ;

            //var NewObservation = new roomUsage
            var RoomNumber = document.getElementById("roomNumber").value;
            var Address = document.getElementById(formIDs[0]).value;
            var SeatsUsed = document.getElementById(formIDs[2]).value;
            var SeatsAvailable = document.getElementById(formIDs[3]).value;
            var Lights = document.getElementById("lights").value;
            var Cooling = document.getElementById("heatingCooling").value;
            var Hours = currentdate.getHours() ;

            //var NewObservation = new roomUsage(RoomNumber, Address, Lights, Cooling, SeatsUsed, SeatsAvailable, datetime);
            var NewObservation = new roomUsage(RoomNumber, Address, Lights, Cooling, SeatsUsed, SeatsAvailable, datetime,Hours)
            console.log(NewObservation)
            //console.log(localStorage.setItem( "NewObservationd",JSON.stringify(NewObservation)));
            var RoomObservation = new roomUsageList(NewObservation)

            console.log(RoomObservation)
            /*
            localStorage.setItem(key, RoomObservation);
            */


            var check = localStorage.getItem(key)

            if (check === null) {
                var observ = ["creating", RoomObservation];
                var obs = JSON.stringify(observ)
                localStorage.setItem(key, obs);
            } else {
                var NewObservationde = JSON.parse(localStorage.getItem(key));
                NewObservationde.push(RoomObservation);

                localStorage.setItem(key, JSON.stringify(NewObservationde));

            };
            


            //localStorage.setItem(key, JSON.stringify(RoomObservation))

        }
        console.log(JSON.parse(localStorage.getItem(key)));
        document.location.reload(true);
    }
}
