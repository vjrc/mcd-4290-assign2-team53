"use strict";
var SavedObservations = JSON.parse(localStorage.getItem(key));
let OutputArea = document.getElementById('content')
let Output = "";
console.log(SavedObservations.length);
console.log(SavedObservations[1].roomList);
let Eight = [];
let nine = [];
let ten = [];
let eleven = [];
let twelve = [];
let thirteen = [];
let fourteen = [];
let fifteen = [];
let sixteen = [];
let seventeen = [];
let Eighteen = [];

for (var k = 1; k < SavedObservations.length; k++) {
    let Hours = SavedObservations[k].roomList.hours;
    if (Hours == 8) {
        Eight.push(k);
    } else if (Hours == 9) {
        nine.push(k);
    } else if (Hours == 10) {
        ten.push(k);
    } else if (Hours == 11) {
        eleven.push(k);
    } else if (Hours == 12) {
        twelve.push(k);
    } else if (Hours == 13) {
        thirteen.push(k);
    } else if (Hours == 14) {
        fourteen.push(k);
    } else if (Hours == 15) {
        fifteen.push(k);
    } else if (Hours == 16) {
        sixteen.push(k);
    } else if (Hours == 17) {
        seventeen.push(k);
    } else if (Hours == 18) {
        Eighteen.push(k);
    }
}
console.log(typeof JSON.parse(SavedObservations[1].roomList.seatsUsed))
console.log(JSON.parse(SavedObservations[1].roomList.seatsUsed)/(JSON.parse(SavedObservations[1].roomList.seatsUsed) + JSON.parse(SavedObservations[1].roomList.seatsTotal)))
if (Eight.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 8 am</h5></th></tr></thead>"
    for (var i = 0; i < Eight.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[Eight[i]].roomList.address+"; Rm "+ SavedObservations[Eight[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[Eight[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[Eight[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[Eight[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[Eight[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[Eight[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[Eight[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (nine.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 9 am</h5></th></tr></thead>"
    for (var i = 0; i < nine.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[nine[i]].roomList.address+"; Rm "+ SavedObservations[nine[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[nine[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[nine[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[nine[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[nine[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[nine[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[nine[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (ten.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 10 am</h5></th></tr></thead>"
    for (var i = 0; i < ten.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[ten[i]].roomList.address+"; Rm "+ SavedObservations[ten[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[ten[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[ten[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[ten[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[ten[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[ten[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[ten[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (eleven.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 11 am </h5></th></tr></thead>"
    for (var i = 0; i < eleven.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[eleven[i]].roomList.address+"; Rm "+ SavedObservations[eleven[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[eleven[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[eleven[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[eleven[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[eleven[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[eleven[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[eleven[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (twelve.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 12 am</h5></th></tr></thead>"
    for (var i = 0; i < twelve.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[twelve[i]].roomList.address+"; Rm "+ SavedObservations[twelve[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[twelve[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[twelve[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[twelve[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[twelve[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[twelve[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[twelve[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (thirteen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 1 pm</h5></th></tr></thead>"
    for (var i = 0; i < thirteen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[thirteen[i]].roomList.address+"; Rm "+ SavedObservations[thirteen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[thirteen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[thirteen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[thirteen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[thirteen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[thirteen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[thirteen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (fourteen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 2 pm </h5></th></tr></thead>"
    for (var i = 0; i < fourteen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[fourteen[i]].roomList.address+"; Rm "+ SavedObservations[fourteen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[fourteen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[fourteen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[fourteen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[fourteen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[fourteen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[fourteen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (fifteen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 3 pm </h5></th></tr></thead>"
    for (var i = 0; i < fifteen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[fifteen[i]].roomList.address+"; Rm "+ SavedObservations[fifteen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[fifteen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[fifteen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[fifteen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[fifteen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[fifteen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[fifteen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (sixteen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 4 pm </h5></th></tr></thead>"
    for (var i = 0; i < sixteen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[sixteen[i]].roomList.address+"; Rm "+ SavedObservations[sixteen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[sixteen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[sixteen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[sixteen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[sixteen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[sixteen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[sixteen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (seventeen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 5 pm </h5></th></tr></thead>"
    for (var i = 0; i < seventeen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[seventeen[i]].roomList.address+"; Rm "+ SavedObservations[seventeen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[seventeen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[seventeen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[seventeen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[seventeen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[seventeen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[seventeen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}
if (Eighteen.length > 0){
    Output += "<div class=\"mdl-cell mdl-cell--4-col\"><table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\"><thead>"
    Output += "<tr><th class=\"mdl-data-table__cell--non-numeric\"><h5> Worst occupancy for 6 pm </h5></th></tr></thead>"
    for (var i = 0; i < Eighteen.length ; i++){
        Output += "<tbody><tr><td class=\"mdl-data-table__cell--non-numeric\"><div><b>"+ SavedObservations[Eighteen[i]].roomList.address+"; Rm "+ SavedObservations[Eighteen[i]].roomList.roomNumber +"</b></div><div>Occupancy: " + Math.floor((JSON.parse(SavedObservations[Eighteen[i]].roomList.seatsUsed)/(JSON.parse(SavedObservations[Eighteen[i]].roomList.seatsUsed) + JSON.parse(SavedObservations[Eighteen[i]].roomList.seatsTotal)) * 100 )) +" %</div><div>Heating/cooling: " + SavedObservations[Eighteen[i]].roomList.heatingCoolingOn+"</div><div>Lights: " + SavedObservations[Eighteen[i]].roomList.lightsOn+"</div><div><font color=\"grey\"><i>" + SavedObservations[Eighteen[i]].roomList.timeChecked+"</i>";

    }
    Output += "</font></div></td></tr></tbody></table></div>"
}

OutputArea.innerHTML = Output;